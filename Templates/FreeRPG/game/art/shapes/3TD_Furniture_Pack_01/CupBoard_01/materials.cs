
singleton Material(Cupboard_01_TypeA_FurniMetals)
{
   mapTo = "FurniMetals";
   diffuseMap[0] = "3TD_MetalBrass_01";
   specular[0] = "0.996078 0.996078 0.996078 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "3.62745";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};

singleton Material(Cupboard_01_TypeA_FurnWood01)
{
   mapTo = "FurnWood01";
   diffuseMap[0] = "3TD_WoodGrain_11";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "66";
   translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};

singleton Material(Cupboard_01_TypeA_MarblTop)
{
   mapTo = "MarblTop";
   diffuseMap[0] = "3TD_MARL_10";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "56";
   translucentBlendOp = "None";
   specularStrength[0] = "0.490196";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   subSurface[0] = "1";
   subSurfaceColor[0] = "0.560784 0.556863 0.556863 1";
};

singleton Material(Cupboard_01_TypeA_FurnWood02)
{
   mapTo = "FurnWood02";
   diffuseMap[0] = "3TD_WoodGrain_11a";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "2.7451";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};

singleton Material(Cupboard_01_TypeA_FurniColColor)
{
   mapTo = "FurniColColor";
   diffuseColor[0] = "0.588 0.588 0.588 1";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};
