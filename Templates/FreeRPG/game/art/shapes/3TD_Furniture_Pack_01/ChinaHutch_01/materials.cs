
singleton Material(ChinaHutch_01_TypeA_FurintureMetals)
{
   mapTo = "FurintureMetals";
   diffuseMap[0] = "3TD_MetalBrass_01";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "3.62745";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};

singleton Material(ChinaHutch_01_TypeA__td_Glass)
{
   mapTo = "_td_Glass";
   diffuseColor[0] = "0 0 0 1";
   specularPower[0] = "66";
   translucent = "1";
   doubleSided = "1";
   translucentBlendOp = "Add";
   alphaRef = "0";
   cubemap = "DesertSkyCubemap";
   pixelSpecular[0] = "1";
};

singleton Material(ChinaHutch_01_TypeA_ChinaHutch_01_horz)
{
   mapTo = "ChinaHutch_01_horz";
   diffuseMap[0] = "3TD_WoodGrain_11";
   specular[0] = "0.996078 0.996078 0.996078 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "1.47059";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};

singleton Material(ChinaHutch_01_TypeA_ChinaHutch_02_vert)
{
   mapTo = "ChinaHutch_02_vert";
   diffuseMap[0] = "3TD_WoodGrain_11a";
   specular[0] = "0.996078 0.996078 0.996078 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
   specularStrength[0] = "1.76471";
   pixelSpecular[0] = "1";
   doubleSided = "1";
};
