
singleton Material(ladderwoodnew)
{
   mapTo = "Wood";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/Blacksmith/Wood";
   doubleSided = "1";
   translucentBlendOp = "None";
   specular[2] = "White";
};

singleton Material(Blacksmith1Upload_LogEdgeWeathered)
{
   mapTo = "LogEdgeWeathered";
   diffuseMap[0] = "TreeLogEdgeWeathered";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_RustedMetal)
{
   mapTo = "RustedMetal";
   diffuseMap[0] = "Rusted Metal";
   specular[0] = "0.0473684 0.0473684 0.0473684 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_Iron)
{
   mapTo = "Iron";
   diffuseMap[0] = "Iron";
   specular[0] = "0.164268 0.164268 0.164268 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_WindowBlue1)
{
   mapTo = "WindowBlue1";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/Blacksmith/WindowBlue1";
   specular[0] = "0.025 0.025 0.025 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(Blacksmith1Upload_WindowBlue2)
{
   mapTo = "WindowBlue2";
   diffuseMap[0] = "WindowsBlue2";
   specular[0] = "0.025 0.025 0.025 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_WroughtIron)
{
   mapTo = "WroughtIron";
   diffuseMap[0] = "WroughtIron";
   specular[0] = "0.0473684 0.0473684 0.0473684 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_GreyStone2)
{
   mapTo = "GreyStone2";
   diffuseMap[0] = "GreyStone2";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_Bark)
{
   mapTo = "Bark";
   diffuseMap[0] = "Bark";
   specular[0] = "0.0157895 0.0157895 0.0157895 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_LogEdge)
{
   mapTo = "LogEdge";
   diffuseMap[0] = "TreeLogEdge";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_RoofTiles)
{
   mapTo = "RoofTiles";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/Blacksmith/RoofTiles";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(Blacksmith1Upload_Plaster)
{
   mapTo = "Plaster";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/Blacksmith/PlasterLarge";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(Blacksmith1Upload_Plaster)
{
   mapTo = "Plaster";
   diffuseMap[0] = "PlasterLarge";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_GreyBrick2)
{
   mapTo = "GreyBrick2";
   diffuseMap[0] = "GreyBricks2";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_HotIron)
{
   mapTo = "HotIron";
   diffuseMap[0] = "WroughtIronHot";
   specular[0] = "0.131579 0.131579 0.131579 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_Sandstone)
{
   mapTo = "Sandstone";
   diffuseMap[0] = "Sandstone";
   specular[0] = "0.0789474 0.0789474 0.0789474 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_GreyBrick)
{
   mapTo = "GreyBrick";
   diffuseMap[0] = "GreyBricks";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_DoorType1_2)
{
   mapTo = "DoorType1_2";
   diffuseMap[0] = "art/shapes/Daniel74BlendSwap/Buildings/Blacksmith/DoorType1_2";
   specular[0] = "1 1 1 1";
   specularPower[0] = "8";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(Blacksmith1Upload_DoorType1_2)
{
   mapTo = "DoorType1_2";
   diffuseMap[0] = "DoorType1_2";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_Brick)
{
   mapTo = "Brick";
   diffuseMap[0] = "GreyBricks";
   specular[0] = "0.00526315 0.00526315 0.00526315 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_BrownLeather)
{
   mapTo = "BrownLeather";
   diffuseMap[0] = "BrownLeather";
   specular[0] = "0.0380184 0.00789001 0.00270465 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_OakBark)
{
   mapTo = "OakBark";
   diffuseMap[0] = "OakBark";
   specular[0] = "0.0157895 0.0157895 0.0157895 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_Coal)
{
   mapTo = "Coal";
   diffuseMap[0] = "Lava";
   specular[0] = "0.173611 0.173611 0.173611 1";
   specularPower[0] = "128";
   translucentBlendOp = "None";
};

singleton Material(Blacksmith1Upload_ChoppedWood)
{
   mapTo = "ChoppedWood";
   diffuseColor[3] = "White";
   diffuseMap[0] = "ChoppedWood";
   translucentBlendOp = "None";
};
