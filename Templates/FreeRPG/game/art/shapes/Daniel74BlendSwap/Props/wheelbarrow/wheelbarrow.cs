
singleton TSShapeConstructor(WheelbarrowDae)
{
   baseShape = "./wheelbarrow.dae";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
