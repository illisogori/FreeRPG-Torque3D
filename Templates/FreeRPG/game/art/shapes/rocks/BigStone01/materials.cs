
singleton Material(BigStone01_Rock01a_jpg)
{
   mapTo = "Rock01a_jpg";
   diffuseMap[0] = "3td_Stone_01_512.png";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   normalMap[0] = "3td_Stone_01_512_NRM.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
