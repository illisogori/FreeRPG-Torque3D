

singleton Material(misc_groundshrubs_01)
{
   mapTo = "misc_groundshrubs_01";
   diffuseColor[0] = "0.776471 0.8 0.65098 1";
   diffuseMap[0] = "art/shapes/bushes/groundshrubs/misc_groundshrubs_01/misc_groundshrubs_01_diffuse.dds";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   alphaTest = "1";
   alphaRef = "60";
   materialTag1 = "Plant";
   materialTag0 = "Branch";
   subSurface[0] = "1";
   subSurfaceColor[0] = "0.427451 0.486275 0.266667 1";
};
