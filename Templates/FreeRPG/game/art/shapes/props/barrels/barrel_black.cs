
singleton TSShapeConstructor(Barrel_blackDAE)
{
   baseShape = "./barrel_black.DAE";
   adjustCenter = "1";
   adjustFloor = "1";
   loadLights = "0";
};
