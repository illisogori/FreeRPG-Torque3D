
singleton Material(rope_fence01_Rope)
{
   mapTo = "Rope";
   diffuseMap[0] = "rope_diffuse";
   normalMap[0] = "rope_normal";
   translucentBlendOp = "None";
};

singleton Material(rope_fence01_PostMat)
{
   mapTo = "PostMat";
   diffuseMap[0] = "rope_fence_post_color";
   translucentBlendOp = "None";
};
