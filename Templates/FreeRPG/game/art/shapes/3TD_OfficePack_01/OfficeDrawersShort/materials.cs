
singleton Material(OfficeDrawersShort_OfficeLiteHoz)
{
   mapTo = "OfficeLiteHoz";
   diffuseMap[0] = "3TD_Laminate_hoz";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(OfficeDrawersShort_OfficeBlack)
{
   mapTo = "OfficeBlack";
   diffuseMap[0] = "3TD_BlackLaminate";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   specularStrength[0] = ".2";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};
