
singleton Material(Pup_Tent_Material_191)
{
   mapTo = "Material_191";
   diffuseColor[0] = "0.609018 0.539074 0.439698 1";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_192)
{
   mapTo = "Material_192";
   diffuseColor[0] = "0.64 0.64 0.64 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_021)
{
   mapTo = "Material_021";
   diffuseColor[0] = "0.0998175 0.0935765 0.079919 1";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_005)
{
   mapTo = "Material_005";
   diffuseColor[0] = "0.548765 0.422312 0.180664 1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_002)
{
   mapTo = "Material_002";
   diffuseColor[0] = "0.12186 0.075891 0.0209336 1";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_003)
{
   mapTo = "Material_003";
   diffuseColor[0] = "0.196955 0.121267 0.0317956 1";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_004)
{
   mapTo = "Material_004";
   diffuseColor[0] = "0.110067 0.0778262 0.0419408 1";
   doubleSided = "1";
   translucentBlendOp = "None";
};

singleton Material(Pup_Tent_Material_189)
{
   mapTo = "Material_189";
   diffuseColor[0] = "0.64 0.64 0.64 0.891089";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucent = "1";
};

singleton Material(mediaeval_house_Mittel_Haus)
{
   mapTo = "Mittel_Haus";
   diffuseMap[0] = "art/shapes/blendswap/MittelH col DH.jp.jpg";
   normalMap[0] = "art/shapes/blendswap/MittelH nor DH.jpg.jp.jpg";
   doubleSided = "1";
   translucentBlendOp = "None";
};
