
singleton Material(Cave_02_Stone24)
{
   mapTo = "Stone24";
   diffuseMap[0] = "3td_stone_015.jpg";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "101";
   translucentBlendOp = "None";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   normalMap[0] = "3TD_Blocky_SL_NRM.png";
   materialTag0 = "RoadAndPath";
   pixelSpecular[0] = "1";
};
