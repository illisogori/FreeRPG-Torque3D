
singleton Material(Cave_Y_Tube_01_Stone24jpg)
{
   mapTo = "Stone24jpg";
   diffuseMap[0] = "3td_stone_015.jpg";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "101";
   translucentBlendOp = "None";
   normalMap[0] = "3td_CliffSet_01_NRM.png";
   pixelSpecular[0] = "1";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   materialTag0 = "RoadAndPath";
};
