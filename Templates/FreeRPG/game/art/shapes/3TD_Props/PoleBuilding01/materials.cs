
singleton Material(PoleBuilding01_WoodPlank02a)
{
   mapTo = "WoodPlank02a";
   diffuseMap[0] = "3td_WoodPlank_02";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   normalMap[0] = "3td_WoodPlank_02_NRM.png";
   useAnisotropic[0] = "1";
   doubleSided = "1";
};

singleton Material(polebuildingwoodroof)
{
   mapTo = "CorrigateRust03";
   diffuseMap[0] = "art/textures/wood/planks/wood_paneling_grey_01_diffuse.dds";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
   normalMap[0] = "art/textures/wood/planks/wood_paneling_grey_01_normal.dds";
   useAnisotropic[0] = "1";
   doubleSided = "1";
   pixelSpecular[0] = "0";
   specularMap[0] = "art/textures/wood/planks/wood_paneling_grey_01_specular.dds";
};

singleton Material(PoleBuilding01__3___Default)
{
   mapTo = "_3_-_Default";
   diffuseColor[0] = "0.588 0.588 0.588 1";
   specular[0] = "0.9 0.9 0.9 1";
   specularPower[0] = "10";
   translucentBlendOp = "None";
};
