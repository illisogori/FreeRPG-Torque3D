//-----------------------------------------------------------------------------
// Torque
// Copyright GarageGames, LLC 2011
//-----------------------------------------------------------------------------

singleton TSShapeConstructor(VolcanoDAE)
{
   baseShape = "./volcano.DAE";
   loadLights = "0";
};
