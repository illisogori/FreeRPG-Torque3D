
singleton Material(house01_Wood_005)
{
   mapTo = "Wood_005";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.004";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_WindowDark2_001)
{
   mapTo = "WindowDark2_001";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/WindowsBlue2";
   specular[0] = "0.0125 0.0125 0.0125 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_Wood_007)
{
   mapTo = "Wood_007";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.004";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_WindowYellow1)
{
   mapTo = "WindowYellow1";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/WindowAllBroken1";
   specular[0] = "0.0125 0.0125 0.0125 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_DarkPlaster)
{
   mapTo = "DarkPlaster";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/DarkPlaster";
   specular[0] = "0.0386179 0.0386179 0.0386179 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_WindowDark1)
{
   mapTo = "WindowDark1";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/WindowsBlue2";
   specular[0] = "0.0125 0.0125 0.0125 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_Wood_004)
{
   mapTo = "Wood_004";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.004";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_Wood_006)
{
   mapTo = "Wood_006";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.004";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_Wood_002)
{
   mapTo = "Wood_002";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.002";
   specular[0] = "0.000137271 5.25275e-005 3.49553e-005 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_RoofTiles_002)
{
   mapTo = "RoofTiles_002";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/RoofTiles.jpg.002";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
};

singleton Material(house01_RustedMetal_001)
{
   mapTo = "RustedMetal_001";
   diffuseColor[2] = "White";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Rusted Metal.jpg.001";
   specular[0] = "0.0236842 0.0236842 0.0236842 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   alphaRef = "60";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house01_Wood_008)
{
   mapTo = "Wood_008";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.005";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house01_Plaster_001)
{
   mapTo = "Plaster_001";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Plaster.jpg.001";
   specular[0] = "0.0131579 0.0131579 0.0131579 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house01_RoofTiles_001)
{
   mapTo = "RoofTiles_001";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/RoofTiles.jpg.001";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house01_Wood_003)
{
   mapTo = "Wood_003";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Wood.jpg.003";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house01_RoofTiles_003)
{
   mapTo = "RoofTiles_003";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/RoofTiles.jpg.003";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house12_RustedMetal_002)
{
   mapTo = "RustedMetal_002";
   diffuseColor[3] = "White";
   diffuseMap[0] = "Rusted Metal.jpg.001";
   specular[0] = "0.0236842 0.0236842 0.0236842 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house12_DoorType1_2_001)
{
   mapTo = "DoorType1_2_001";
   diffuseMap[0] = "DoorType1_1.jpg.001";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house12_Stone_001)
{
   mapTo = "Stone_001";
   diffuseMap[0] = "Stone";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house24_Plaster_002)
{
   mapTo = "Plaster_002";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/Plaster.jpg.001";
   specular[0] = "0.0386179 0.0386179 0.0386179 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house24_GreyBricks_001)
{
   mapTo = "GreyBricks_001";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/GreyBricks";
   specular[0] = "0.0131579 0.0131579 0.0131579 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house24_WindowDark2)
{
   mapTo = "WindowDark2";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/WindowsBlue2";
   specular[0] = "0.0125 0.0125 0.0125 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Terrain";
   terrainMaterials = "1";
};

singleton Material(house28_Plaster_003)
{
   mapTo = "Plaster_003";
   diffuseMap[0] = "Plaster.jpg.001";
   specular[0] = "0.0386179 0.0386179 0.0386179 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house28_Plaster_004)
{
   mapTo = "Plaster_004";
   diffuseMap[0] = "Plaster.jpg.001";
   specular[0] = "0.0386179 0.0386179 0.0386179 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_Wood_009)
{
   mapTo = "Wood_009";
   diffuseMap[0] = "Wood.jpg.005";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_Wood_011)
{
   mapTo = "Wood_011";
   diffuseMap[0] = "Wood.jpg.005";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_Stone_002)
{
   mapTo = "Stone_002";
   diffuseMap[0] = "Stone";
   specular[0] = "0.5 0.5 0.5 1";
   specular[3] = "White";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_DoorType1_1_001)
{
   mapTo = "DoorType1_1_001";
   diffuseMap[0] = "DoorType1_1";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_Stone)
{
   mapTo = "Stone";
   diffuseMap[0] = "Stone";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_RustedMetal_003)
{
   mapTo = "RustedMetal_003";
   diffuseMap[0] = "Rusted Metal.jpg.001";
   specular[0] = "0.0236842 0.0236842 0.0236842 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house38_Wood_010)
{
   mapTo = "Wood_010";
   diffuseMap[0] = "Wood.jpg.005";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house61_GreyBricks)
{
   mapTo = "GreyBricks";
   diffuseMap[0] = "GreyBricks";
   specular[0] = "0.0131579 0.0131579 0.0131579 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(thouse04_Plaster_005)
{
   mapTo = "Plaster_005";
   diffuseMap[0] = "Plaster";
   specular[0] = "0.0131579 0.0131579 0.0131579 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(thouse04_RoofTiles_007)
{
   mapTo = "RoofTiles_007";
   diffuseMap[0] = "RoofTiles";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(thouse04_Plaster_007)
{
   mapTo = "Plaster_007";
   diffuseMap[0] = "Plaster";
   specular[0] = "0.0131579 0.0131579 0.0131579 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(thouse04_RoofTiles_005)
{
   mapTo = "RoofTiles_005";
   diffuseMap[0] = "RoofTiles";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(house66_WindowDark2_002)
{
   mapTo = "WindowDark2_002";
   diffuseMap[0] = "art/shapes/ModularMedievalBuildings/CREATEDEXPORT/WindowAllBroken1";
   specular[0] = "0.0125 0.0125 0.0125 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
   doubleSided = "1";
   materialTag0 = "Rock";
};

singleton Material(thouse24_RoofTiles_006)
{
   mapTo = "RoofTiles_006";
   diffuseMap[0] = "RoofTiles.jpg.005";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(thouse24_RoofTiles_004)
{
   mapTo = "RoofTiles_004";
   diffuseMap[0] = "RoofTiles.jpg.004";
   specular[0] = "0.00101947 0.00101947 0.00101947 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};

singleton Material(thouse24_Wood_012)
{
   mapTo = "Wood_012";
   diffuseMap[0] = "Wood.jpg.007";
   specular[0] = "0.5 0.5 0.5 1";
   specularPower[0] = "50";
   translucentBlendOp = "None";
};
