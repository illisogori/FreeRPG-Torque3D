
singleton Material(ekasia_tumula_01_bark)
{
   mapTo = "ekasia_tumula_01_bark";
   diffuseColor[0] = "0.6 0.58 0.45 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "16";
   doubleSided = "1";
   translucentBlendOp = "None";
   diffuseMap[0] = "art/shapes/trees/fantasy/ekasia_tumula_01/ekasia_tumula_01_bark_diffuse.dds";
   normalMap[0] = "art/shapes/trees/fantasy/ekasia_tumula_01/ekasia_tumula_01_bark_normal.dds";
   specularMap[0] = "art/shapes/trees/fantasy/ekasia_tumula_01/ekasia_tumula_01_bark_diffuse.dds";
   materialTag0 = "Bark";
};

singleton Material(ekasia_tumula_01_branch)
{
   mapTo = "ekasia_tumula_01_branch";
   diffuseColor[0] = "0.996078 0.996078 0.996078 1";
   specular[0] = "0 0 0 1";
   specularPower[0] = "50";
   doubleSided = "1";
   translucent = "0";
   diffuseMap[0] = "art/shapes/trees/fantasy/ekasia_tumula_01/ekasia_tumula_01_branch_diffuse.dds";
   alphaTest = "1";
   alphaRef = "100";
   materialTag0 = "Branch";
};
